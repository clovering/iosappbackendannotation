//$(function() {});等同于$(document).ready()
$(function() {
	$('#item_list tr').hover(function() {
		$(this).addClass('over');
	}, function() {
		$(this).removeClass('over');
	})
})

$(function() {
	$('.recommend a').click(function() {
		var id = $(this).parents('tr').attr('id');
		var act = $(this).attr('class');
		var _this = $(this);
		$.post('/iappbackend/ajaxRecommend', {
			id : id,
			act : act
		}, function(d) {
			if (d.stat == 1) {
				$(_this).parent().html(d.msg);
			} else {
				alert(d.msg);
			}
		}, 'json');
	});
})

$(function() {
	$('#thumb a').live('click', function() {
		var id = $(this).attr('id');
		var _this = this;
		$.post('/iappbackend/ajaxDeleteImg', {
			id : id
		}, function(data) {
			// $(_this).parent().html(data.msg + "," + data.stat);
			if (data.stat == 1) {
				$(_this).parent().parent().remove();
			} else {
				alert(data.msg);
			}
		}, 'json');
	});
});

$(function() {
	$('#iconThumb a').live('click', function() {
		var id = $(this).attr('id');
		var _this = this;
		$.post('/iappbackend/ajaxDeleteIcon', {
			id : id
		}, function(data) {
			if (data.stat == 1) {
				$(_this).parent().parent().children('img').attr('src', 'upload/xxoo.png');
				$(_this).parent().html(data.msg);
			} else {
				alert(data.msg);
			}
		}, 'json');
	});
});

$(function() {
	$('#upload').click(function() {
		$("#loading").ajaxStart(function() {
			$(this).show();
		}).ajaxComplete(function() {
			$(this).hide();
		});
		
		var appId = $('table.table_form td:first').attr('id');

		$.ajaxFileUpload({
			url : 'ajaxUploadImg.action',
			secureuri : false,
			fileElementId : 'file',
			dataType : 'json',
			data: { 
	            "appId": appId
	        },  
			success : function(data, status) {
				if (data.stat == 1) {
					$('#thumb ul').append("<li><img src=\"upload" + data.photo.originalUrl + "\"/><div><a id=\"" + data.photo.id + "\" href=\"javascript:void(0)\">删除</a></div></li>");
				} else {
					alert(data.msg);
				}
			},
			error : function(data, status, e) {
				alert('error type!');
			}
		});
	});
});

$(function() {
	$('#iconFile').live('change', function() {
		$("#loading").ajaxStart(function() {
			$(this).show();
		}).ajaxComplete(function() {
			$(this).hide();
		});
		
		var appId = $('table.table_form td:first').attr('id');

		$.ajaxFileUpload({
			url : 'ajaxUploadIcon.action',
			secureuri : false,
			fileElementId : 'iconFile',
			dataType : 'json',
			data: { 
	            "appId": appId
	        },  
			success : function(data, status) {
				if (data.stat == 1) {
					$('#iconThumb img').attr('src', 'upload/' + data.iconUrl);
					$('#iconThumb div').html('<a id="' + data.appId + '" href="javascript:void(0)">删除</a>');
				} else {
					alert(data.msg);
				}
			},
			error : function(data, status, e) {
				alert('error type!');
			}
		});
		$('#iconFile').val("");
	});
});

$(function() {
	$('#condition').keyup(function() {
		if ($('#requirement').val() == 'name') {
			var query = $(this).val();
			var _this = this;
			if (query.length >= 2) {
				$(".listbox").css("top", $(this).offset().top + 18);
				$(".listbox").css("left", $(this).offset().left);	
				$(".listbox").css("display", "block");
				var _this = this;
				$.post('/iappbackend/ajaxSearch', {
					query : query
				}, function(data) {
					if (data.stat == 1) {
						$('.listbox ul').empty();
						$.each(data.apps, function(n, value) {
							$('.listbox ul').append("<li>" + value.name + "</li>"); 
							$('.listbox ul li').hover(function() {
								$(this).addClass('hover');
							}, function() {
								$(this).removeClass('hover');
							});
						});  	
						$('.listbox li').click(function() {
							$(_this).val(this.innerHTML);
							$("form:first").submit();	//提交本页的第一个表单
						});
					} else {
						alert(data.msg);
					}
				}, 'json');
			}
		}
	});
});

$(function() {
	$(document).click(function() {
		$(".listbox").css("display", "none");
	});
});

// 这个能阻止上面方法, 实现了展现ajax下拉框的时候点击除了这个input之外的地方隐藏ajax下拉div
$(function() {
	$('#condition').click(function() {
		return false;
	});
});

function init() {
	var height = $(window).height();
	var width = $(window).width();
	$('#main').height(height);
	$('#frame_container').width(width - 150);
	$('#frame_container iframe').height(height - 70);
}

$(function() {
	init();
})

$(function() {
	$(window).resize(function() {
		init();
	});
})

$(function() {
	$('#left .tree a').click(function() {
		$('#left .tree .itemarea').removeClass('active');
		$(this).parents('.itemarea').addClass('active');
	});
})

function setpos(pos) {
	$('#position span').text(pos);
}

$(function() {
	$("li img").live('mouseover', function() {
		if ($(this).parent().parent().parent().attr('id') == 'iconThumb') {
			$('#original_container img').css('height', '120px');
		}
		$("#original").attr("src", $(this).attr("src"));
		$("#original_container").css("top", $(this).offset().top - 30);
		$("#original_container").css("left", $(this).offset().left + $(this).width() + 6);	
		$("#original_container").css("display", "block");
   });	
   $("li img").live('mouseout', function() {
		$("#original_container").css("display", "none");
		if ($(this).parent().parent().parent().attr('id') == 'iconThumb') {
			$('#original_container img').css('height', '200px');
		}
   });	
});

//$(function() {
//	$("li img").hover(function() {
//		$("#pic_show").attr("src", $(this).attr("src"));
//		$("#absolute_pic").css("top", $(this).offset().top - 30);
//		$("#absolute_pic").css("left", $(this).offset().left + $(this).width() + 6);	
//		$("#absolute_pic").css("display","block");
//   }).mouseout(function() {
//		$("#absolute_pic").css("display","none");
//   });	
//	// 连着写的方式也可以, 分开写也可以
////   $("li img").mouseout(function() {
////		$("#absolute_pic").css("display","none");
////   });	
//});

$(function() {
	$('td.t_c input').keyup(function() {
		var re = /^-?[1-9]*(\d*)?$|^-?0(\d*)?$/;
		if (!re.test($(this).val())) {
			alert('illegal number');
			$(this).val("");
			$(this).focus();	//jquery的focus()方法
			// this.focus();也可以, 这个是用的dom的focus()方法
			return false;
		} else if ($(this).val().length > 9) {
			alert('too long! (length<10)');
			$(this).val("");
			$(this).focus();
			return false;
		}
	});
});

function checkID() {
	if ($('#requirement').val() == 'id') {
		var _this = $('#condition');
		var content = $('#condition').val();
		if (content == "") {
			alert('ID must not be null!');
			return false;
		}
		var re = /^-?[1-9]*(\d*)?$|^-?0(\d*)?$/;
		if (!re.test(content)) {
			alert('illegal number');
			_this.val("");
			_this.focus();
			return false;
		}
		if (content.length != 9) {
			alert("ID length must be 9");
			_this.val("");
			_this.focus();
			return false;
		}
	} else {
		return true;
	}
}

$(function() {
	$('#add_action_app_categoryId').live('change', function() {
		var index = $('#add_action_app_categoryId option').index($('#add_action_app_categoryId option:selected'));
		$('#add_action_app_categoryName').get(0).selectedIndex = index;
	});
});

$(function() {
	$('#add_action_app_categoryName').live('change', function() {
		var index = $('#add_action_app_categoryName option').index($('#add_action_app_categoryName option:selected'));
		$('#add_action_app_categoryId').get(0).selectedIndex = index;
	});
});

$(function() {
	$('#addphoto').live('click', function() {
		var p = $('#photo_container').children();
		if (p.length < 8) {
			$('#photo_container').append('<input type="file" name="photo" style="position:static;width:160px;" />');
		} else {
			alert('max amount 8!');
		}
	});
});

$(function() {
	$('#removephoto').live('click', function() {
		var p = $('#photo_container').children();
		if (p.length > 1) {
			var c = p.get(0);
			$(c).remove();
		} else {
			alert('min amount 1!');
		}
	});
});

jQuery.extend(jQuery.validator.messages, {
    required: "必选字段",
	remote: "请修正该字段",
	email: "请输入正确格式的电子邮件",
	url: "请输入合法的网址",
	date: "请输入合法的日期",
	dateISO: "请输入合法的日期 (ISO).",
	number: "请输入合法的数字",
	digits: "只能输入整数",
	creditcard: "请输入合法的信用卡号",
	equalTo: "请再次输入相同的值",
	accept: "请输入拥有合法后缀名的字符串",
	maxlength: jQuery.validator.format("请输入一个长度最多是 {0} 的字符串"),
	minlength: jQuery.validator.format("请输入一个长度最少是 {0} 的字符串"),
	rangelength: jQuery.validator.format("请输入一个长度介于 {0} 和 {1} 之间的字符串"),
	range: jQuery.validator.format("请输入一个介于 {0} 和 {1} 之间的值"),
	max: jQuery.validator.format("请输入一个最大为 {0} 的值"),
	min: jQuery.validator.format("请输入一个最小为 {0} 的值")
});