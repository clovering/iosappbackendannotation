<%@ page contentType="text/html; charset=UTF-8"%>
<%@ include file="../../inc/header.jsp"%>
<html lang="en">
<head>
<meta charset="utf-8">
<title>无标题文档</title>
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
.STYLE1 {font-size: 12px}
.STYLE3 {font-size: 12px; font-weight: bold; }
.STYLE4 {
	color: #03515d;
	font-size: 12px;
}
.STYLE5 {
	font-size: 12px; 
	font-weight: bold; 
	height: 19px;
	margin-bottom: 4px;
}
.f_r {
	font-family: Arial,Verdana,"宋体";
	font-size: 10px;
	margin: 0;
	padding: 0;
    float: right;
}
a {
    color: #03515D;
    text-decoration: none;
}
a:hover {
    color: #FF3300;
}
-->
</style>

<script>
var  highlightcolor='#c1ebff';
//此处clickcolor只能用win系统颜色代码才能成功,如果用#xxxxxx的代码就不行,还没搞清楚为什么:(
var  clickcolor='#51b2f6';
function  changeto(){
source=event.srcElement;
if  (source.tagName=="TR"||source.tagName=="TABLE")
return;
while(source.tagName!="TD")
source=source.parentElement;
source=source.parentElement;
cs  =  source.children;
//alert(cs.length);
if  (cs[1].style.backgroundColor!=highlightcolor&&source.id!="nc"&&cs[1].style.backgroundColor!=clickcolor)
for(i=0;i<cs.length;i++){
	cs[i].style.backgroundColor=highlightcolor;
}
}

function  changeback(){
if  (event.fromElement.contains(event.toElement)||source.contains(event.toElement)||source.id=="nc")
return
if  (event.toElement!=source&&cs[1].style.backgroundColor!=clickcolor)
//source.style.backgroundColor=originalcolor
for(i=0;i<cs.length;i++){
	cs[i].style.backgroundColor="";
}
}

function  clickto(){
source=event.srcElement;
if  (source.tagName=="TR"||source.tagName=="TABLE")
return;
while(source.tagName!="TD")
source=source.parentElement;
source=source.parentElement;
cs  =  source.children;
//alert(cs.length);
if  (cs[1].style.backgroundColor!=clickcolor&&source.id!="nc")
for(i=0;i<cs.length;i++){
	cs[i].style.backgroundColor=clickcolor;
}
else
for(i=0;i<cs.length;i++){
	cs[i].style.backgroundColor="";
}
}
</script>

</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="30" background="../../images/tab_05.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="12" height="30"><img src="../../images/tab_03.gif" width="12" height="30" /></td>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="46%" valign="middle"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="5%"><div align="center"><img src="../../images/tb.gif" width="16" height="16" style="margin-top: 6px" /></div></td>
                <td width="95%" class="STYLE1">
                	<div id="search" class="STYLE5">
						<form:form action="search" method="get" onsubmit="return checkID()" command="appSearch" style="height: 16px">
							搜索应用：
							<select name="field" id="requirement">
								<option value="name">
									按名称
								</option>
								<option value="id">
									按ID
								</option>
							</select>
							<input type="text" name="query" class="query" id="condition" />
							<input type="submit" class="button" />
						</form:form>
					</div>
                </td>
              </tr>
            </table></td>
            <td width="54%"><table border="0" align="right" cellpadding="0" cellspacing="0">
              <tr>
                <td width="60"><table width="90%" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td class="STYLE1"><div align="center"><img src="../../images/22.gif" width="14" height="14" /></div></td>
                    <td class="STYLE1"><div align="center"><a href="toAdminAdd.action" target="I2">新增</a></div></td>
                  </tr>
                </table></td>
              </tr>
            </table></td>
          </tr>
        </table></td>
        <td width="16"><img src="../../images/tab_07.gif" width="16" height="30" /></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="8" background="../../images/tab_12.gif">&nbsp;</td>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="b5d6e6" onmouseover="changeto()"  onmouseout="changeback()">
          <tr>
            <td width="8%" height="22" background="../../images/bg.gif" bgcolor="#FFFFFF"><div align="center"><span class="STYLE1">ID</span></div></td>
            <td width="33%" height="22" background="../../images/bg.gif" bgcolor="#FFFFFF"><div align="center"><span class="STYLE1">应用名称</span></div></td>
            <td width="9%" height="22" background="../../images/bg.gif" bgcolor="#FFFFFF"><div align="center"><span class="STYLE1">所属类别</span></div></td>
            <td width="7%" height="22" background="../../images/bg.gif" bgcolor="#FFFFFF"><div align="center"><span class="STYLE1">当前价格</span></div></td>
            <td width="7%" height="22" background="../../images/bg.gif" bgcolor="#FFFFFF"><div align="center"><span class="STYLE1">历史价格</span></div></td>
            <td width="7%" height="22" background="../../images/bg.gif" bgcolor="#FFFFFF"><div align="center"><span class="STYLE1">价格类型</span></div></td>
            <td width="5%" background="../../images/bg.gif" bgcolor="#FFFFFF"><div align="center"><span class="STYLE1">推荐操作</span></div></td>
            <td width="7%" height="22" background="../../images/bg.gif" bgcolor="#FFFFFF"><div align="center"><span class="STYLE1">下载次数</span></div></td>
            <td width="7%" height="22" background="../../images/bg.gif" bgcolor="#FFFFFF"><div align="center"><span class="STYLE1">排序</span></div></td>
            <td width="15%" height="22" background="../../images/bg.gif" bgcolor="#FFFFFF" class="STYLE1"><div align="center">管理操作</div></td>
          </tr>
          <c:forEach items="${ page.items }" var="app" varStatus="status">
          <tr id="${ app.applicationId }">
            <td height="20" bgcolor="#FFFFFF"><div align="center" class="STYLE1">
              <div align="center">${ app.applicationId }</div>
            </div></td>
            <td height="20" bgcolor="#FFFFFF"><div align="center"><span class="STYLE1"><a href="http://www.candou.com/iphone/${ app.applicationId }" target="_blank">${ app.name }</a></span></div></td>
            <td height="20" bgcolor="#FFFFFF"><div align="center"><span class="STYLE1">${ app.categoryName }</span></div></td>
            <td height="20" bgcolor="#FFFFFF"><div align="center"><span class="STYLE1">${ app.currentPrice }</span></div></td>
            <td height="20" bgcolor="#FFFFFF"><div align="center"><span class="STYLE1">${ app.lastPrice }</span></div></td>
            <td height="20" bgcolor="#FFFFFF"><div align="center"><span class="STYLE1">${ app.priceTrend }</span></div></td>
            <td bgcolor="#FFFFFF"><div align="center"><span class="STYLE1"><c:if test="${app.isRecommend == true }">
							<span class="recommend"><a href="javascript:void(0)"
								class="undo">取消推荐</a>
							</span>
						</c:if>
						<c:if test="${app.isRecommend == false }">
							<span class="recommend"><a href="javascript:void(0)"
								class="do">推荐</a>
							</span>
						</c:if></span></div></td>
            <td height="20" bgcolor="#FFFFFF"><div align="center"><span class="STYLE1">${ app.downloads }</span></div></td>
            <td height="20" bgcolor="#FFFFFF"><div align="center"><span class="STYLE1"><input type="text" size="5"
							value="${ app.displayOrder }"
							name="display[${ app.applicationId }]" /></span></div></td>
            <td height="20" bgcolor="#FFFFFF"><div align="center"><span class="STYLE4"><img src="../../images/edt.gif" width="16" height="16" /><a href="${ app.applicationId }">编辑</a>&nbsp;<img src="../../images/del.gif" width="16" height="16" /><a href="${ app.applicationId }" onclick="return confirm('确定要删除?')">删除</a></span></div></td>
          </tr> 
          </c:forEach>
        </table></td>
        <td width="8" background="../../images/tab_15.gif">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="35" background="../../images/tab_19.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="12" height="35"><img src="../../images/tab_18.gif" width="12" height="35" /></td>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td class="STYLE4">&nbsp;&nbsp;共有 ${ count } 条记录&nbsp;&nbsp;&nbsp;
					<%--<input type="submit" value="批量修改显示顺序" class="button" />--%></td>
            <td><table border="0" align="right" cellpadding="0" cellspacing="0">
                <tr>
				<p class="f_r">
					<common:pageV2 url="/admin/app/${from }" optimize="true"/>
                </p>
                </tr>
            </table></td>
          </tr>
        </table></td>
        <td width="16"><img src="../../images/tab_20.gif" width="16" height="35" /></td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>
