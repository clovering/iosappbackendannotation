package com.jiangchao.iakend.model.admin;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class Manager {

	private String name;
	private String password;
	
}
