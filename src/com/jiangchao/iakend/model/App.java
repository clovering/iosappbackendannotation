package com.jiangchao.iakend.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@Entity
@Table(name = "tb_application")
public class App implements Serializable {

	//@Transient表示不持久化改字段
	@Transient
	private static final long serialVersionUID = 7112727770328322665L;

	@Id
	@Column(length = 11, nullable = false, name = "application_id")
	private int applicationId = 0;

	private String name;

	private String alias;

	@Column(name = "release_date")
	private String releaseDate;

	@Column(name = "release_notes")
	private String releaseNotes;

	@Column(name = "system_requirements")
	private String systemRequirements;

	@Column(name = "current_version", columnDefinition = "CHAR(20)")
	private String currentVersion;

	@Column(name = "rating_current_version")
	private int ratingOfCurrentVersion;

	@Column(name = "rating_overall")
	private int ratingOfOverall;

	@Column(name = "star_current_version")
	private float StarOfCurrentVersion;

	@Column(name = "star_overall")
	private float StarOfOverall;

	@Column(name = "current_price")
	private float currentPrice;

	@Column(name = "price_currency")
	private String priceCurrency;

	@Column(name = "last_price")
	private float lastPrice;

	@Column(name = "price_trend", columnDefinition = "CHAR(10)")
	private String priceTrend;

	@Column(name = "expire_datetime")
	private String freeExpireDate;

	@Column(name = "category_id")
	private int categoryId;

	@Column(name = "category_name")
	private String categoryName;

	@Column(name = "file_size")
	private float fileSize;

	@Column(name = "discription_original")
	private String descriptionOriginal;

	@Column(name = "discription")
	private String description;

	@Column(length = 20, columnDefinition = "CHAR(2)")
	private String country;

	private String language;

	@Column(name = "icon_url")
	private String iconUrl;

	@Column(name = "itunes_url")
	private String itunesUrl;

	@Column(name = "seller_id")
	private int sellerId;

	@Column(name = "seller_name")
	private String sellerName;

	private int downloads;

	@Column(name = "support_iphone", columnDefinition = "TINYINT")
	private boolean supportIphone;

	@Column(name = "support_ipad", columnDefinition = "TINYINT")
	private boolean supportIpad;

	@Column(name = "support_chinese", columnDefinition = "TINYINT")
	private boolean supportChinese;

	@Column(name = "support_english", columnDefinition = "TINYINT")
	private boolean supportEnglish;
	
	@Column(name = "status", columnDefinition = "CHAR(10)")
	private String status;

	@Column(name = "created_at")
	private String createdAt;

	@Column(name = "updated_at")
	private String updatedAt;
	
	@Column(name = "display_order")
	private int displayOrder;
	
	@Column(name = "is_recommend", columnDefinition = "TINYINT")
	private boolean isRecommend;
	
	public App(String priceTrend) {
		this.priceTrend = priceTrend;
	}

	public boolean getIsRecommend() {
		return isRecommend;
	}

	public void setIsRecommend(boolean isRecommend) {
		this.isRecommend = isRecommend;
	}

}
