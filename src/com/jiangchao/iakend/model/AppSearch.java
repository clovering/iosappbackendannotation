package com.jiangchao.iakend.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class AppSearch {

	private String field;
	
	private String query;
	
}
