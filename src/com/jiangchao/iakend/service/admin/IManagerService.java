package com.jiangchao.iakend.service.admin;

import org.springframework.transaction.annotation.Transactional;

import com.jiangchao.iakend.model.admin.Manager;

@Transactional
public interface IManagerService {

	boolean valid(Manager manager);
	
}
