package com.jiangchao.iakend.service.admin;

import java.util.List;

import com.jiangchao.iakend.model.App;
import com.jiangchao.iakend.model.AppSearch;
import com.jiangchao.scaffold.pagination.Page;
import com.jiangchao.scaffold.service.IBaseService;

public interface IAppService extends IBaseService<App, Integer> {
	
	public List<App> query(AppSearch appSearch);
	
	public int countQuery(AppSearch appSearch);
	
	public List<App> listLimited();
	
	public List<App> listSales();
	
	public List<App> listFree();
	
	public List<App> listRecommend();
	
	public int countLimited();
	
	public int countSales();
	
	public int countFree();
	
	public int countRecommend();

	public Page<App> listLimited(int pn);
	
	public Page<App> listSales(int pn);

	public Page<App> listFree(int pn);

	public Page<App> listRecommend(int pn);

}
