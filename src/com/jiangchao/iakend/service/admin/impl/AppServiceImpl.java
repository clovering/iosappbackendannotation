package com.jiangchao.iakend.service.admin.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.jiangchao.iakend.dao.IAppDao;
import com.jiangchao.iakend.model.App;
import com.jiangchao.iakend.model.AppQuery;
import com.jiangchao.iakend.model.AppSearch;
import com.jiangchao.iakend.service.admin.IAppService;
import com.jiangchao.scaffold.Constants;
import com.jiangchao.scaffold.dao.IBaseDao;
import com.jiangchao.scaffold.pagination.Page;
import com.jiangchao.scaffold.pagination.PageUtil;
import com.jiangchao.scaffold.service.impl.BaseServiceImpl;

@Service("appService")
public class AppServiceImpl extends BaseServiceImpl<App, Integer> implements IAppService {
	
	private IAppDao appDao;

	@Autowired
	@Qualifier("appDao")
	@Override
	public void setBaseDao(IBaseDao<App, Integer> appDao) {
		this.baseDao = appDao;
		this.appDao = (IAppDao) appDao;
	}
	
	@Override
	public int countQuery(AppSearch appSearch) {
		int count = 0;
		if (appSearch.getField().equals("name")) {
			count = appDao.countyByName(appSearch);
		} else {
			count = appDao.countByApplicationId(appSearch);
		}
		return count;
	}
	
	@Override
	public List<App> query(AppSearch appSearch) {
		List<App> appList = null;
		if (appSearch.getField().equals("name")) {
			appList = appDao.queryByName(appSearch);
		} else {
			appList = appDao.queryByApplicationId(appSearch);
		}
		return appList;
	}

	@Override
	public List<App> listLimited() {
		return appDao.listByPriceTrend(new AppQuery("limited"));
	}

	@Override
	public List<App> listSales() {
		return appDao.listByPriceTrend(new AppQuery("sales"));
	}

	@Override
	public List<App> listFree() {
		return appDao.listByPriceTrend(new AppQuery("free"));
	}

	@Override
	public List<App> listRecommend() {
		return appDao.listByPriceTrend(new AppQuery("recommend"));
	}

	@Override
	public int countLimited() {
		return appDao.countByPriceTrend(new AppQuery("limited"));
	}

	@Override
	public int countSales() {
		return appDao.countByPriceTrend(new AppQuery("sales"));
	}

	@Override
	public int countFree() {
		return appDao.countByPriceTrend(new AppQuery("free"));
	}

	@Override
	public int countRecommend() {
		return appDao.countByPriceTrend(new AppQuery("recommend"));
	}

	@Override
	public Page<App> listLimited(int pn) {
		Integer count = countLimited();
		List<App> items = appDao.listByPriceTrend(pn, Constants.DEFAULT_PAGE_SIZE, new AppQuery("limited"));
		return PageUtil.getPage(count, pn, items, Constants.DEFAULT_PAGE_SIZE);
	}

	@Override
	public Page<App> listSales(int pn) {
		Integer count = countSales();
		List<App> items = appDao.listByPriceTrend(pn, Constants.DEFAULT_PAGE_SIZE, new AppQuery("sales"));
		return PageUtil.getPage(count, pn, items, Constants.DEFAULT_PAGE_SIZE);
	}

	@Override
	public Page<App> listFree(int pn) {
		Integer count = countFree();
		List<App> items = appDao.listByPriceTrend(pn, Constants.DEFAULT_PAGE_SIZE, new AppQuery("free"));
		return PageUtil.getPage(count, pn, items, Constants.DEFAULT_PAGE_SIZE);
	}

	@Override
	public Page<App> listRecommend(int pn) {
		Integer count = countRecommend();
		List<App> items = appDao.listByPriceTrend(pn, Constants.DEFAULT_PAGE_SIZE, new AppQuery("recommend"));
		return PageUtil.getPage(count, pn, items, Constants.DEFAULT_PAGE_SIZE);
	}
	
}
