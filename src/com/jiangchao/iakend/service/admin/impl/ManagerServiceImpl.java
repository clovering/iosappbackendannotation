package com.jiangchao.iakend.service.admin.impl;

import org.springframework.stereotype.Service;

import com.jiangchao.iakend.model.admin.Manager;
import com.jiangchao.iakend.service.admin.IManagerService;

@Service("managerService")
public class ManagerServiceImpl implements IManagerService {

	@Override
	public boolean valid(Manager manager) {
		if (manager.getName().equalsIgnoreCase("night") && manager.getPassword().equalsIgnoreCase("raven")) {
			return true;
		}
		return false;
	}

}
