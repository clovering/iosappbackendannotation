package com.jiangchao.iakend.web.admin.controller;

import java.util.Map;

import lombok.extern.log4j.Log4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.jiangchao.iakend.Constants;
import com.jiangchao.iakend.model.admin.Manager;
import com.jiangchao.iakend.service.admin.IManagerService;

@Controller
// @RequestMapping("/admin/login")
@Log4j
@SessionAttributes("userName")
public class LoginController {

	@Autowired
	private IManagerService managerService;

	/**
	 * 初始化表单 GET
	 */
	@RequestMapping(value = "/admin/login", method = RequestMethod.GET)
	public String toLogin(ModelMap model) {
		Manager manager = new Manager();
		model.addAttribute(Constants.COMMAND_MANAGER, manager);
		return "/admin/login";
	}

	/**
	 * 登录 POST
	 */
	@RequestMapping(value = "/admin/login", method = RequestMethod.POST)
	public String login(@ModelAttribute(Constants.COMMAND_MANAGER) Manager manager, ModelMap model) {
		log.debug(manager);
		if (managerService.valid(manager)) {
			model.put("userName", manager.getName());
			return "redirect:/admin/app/index";
		}
		return "redirect:/admin/login";
	}
	
	/**
	 * 退出GET
	 */
	@RequestMapping(value="/admin/logout", method = RequestMethod.GET)
	public String logout(Map<String, Object> model) {
		model.remove("userName");
		return "redirect:/admin/login";
	}

}
