package com.jiangchao.iakend.web.admin.controller;

import javax.servlet.http.HttpServletRequest;

import lombok.extern.log4j.Log4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.jiangchao.iakend.model.App;
import com.jiangchao.iakend.model.AppSearch;
import com.jiangchao.iakend.service.admin.IAppService;
import com.jiangchao.scaffold.Constants;
import com.jiangchao.scaffold.pagination.Page;
import com.jiangchao.scaffold.pagination.PageUtil;

@Controller
@Log4j
@RequestMapping("admin/app")
public class AppController {
	
	@Autowired
	@Qualifier("appService")
	private IAppService appService;
	
	// search
	@RequestMapping(value = "/search", method = { RequestMethod.GET })
	public ModelAndView search(ModelMap model, @ModelAttribute("appSearch") AppSearch command) {
		log.debug(command);
//		model.put("appList", appService.query(command));
		// 查询出来的内容就暂时先不分页了，但是需要构造Page对象 FIXME
		model.put("page", PageUtil.getPage(0, 0, appService.query(command), Constants.DEFAULT_PAGE_SIZE));
		
		model.put("count", appService.countQuery(command));
		return new ModelAndView("/admin/app/layout").addObject(model);
	}
		
	//list
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public String list(HttpServletRequest request, ModelMap model) {
		int pn = ServletRequestUtils.getIntParameter(request, "pn", 1);
		Page<App> page = appService.listAll(pn);
		request.setAttribute("page", page);
		request.setAttribute("count", appService.countAll());
		request.setAttribute("from", "list");
		return "/admin/app/layout";
	}
	
	@RequestMapping(value = "/limited", method = RequestMethod.GET)
	public String limited(HttpServletRequest request, ModelMap model) {
		int pn = ServletRequestUtils.getIntParameter(request, "pn", 1);
		Page<App> page = appService.listLimited(pn);
		request.setAttribute("page", page);
		request.setAttribute("count", appService.countLimited());
		request.setAttribute("from", "limited");
		return "/admin/app/layout";
	}
	
	@RequestMapping(value = "/sales", method = RequestMethod.GET)
	public String sales(HttpServletRequest request, ModelMap model) {
		int pn = ServletRequestUtils.getIntParameter(request, "pn", 1);
		Page<App> page = null;
		page = appService.listSales(pn);
		request.setAttribute("page", page);
		request.setAttribute("count", appService.countSales());
		request.setAttribute("from", "sales");
		return "/admin/app/layout";
	}
	
	@RequestMapping(value = "/free", method = RequestMethod.GET)
	public ModelAndView free(@RequestParam(value = "pn", defaultValue = "1") int pn, HttpServletRequest request,
			ModelMap model) {
		// int pn = ServletRequestUtils.getIntParameter(request, "pn", 1);
		Page<App> page = appService.listFree(pn);
		model.put("page", page);
		model.put("count", appService.countFree());
		model.put("from", "free");
		return new ModelAndView("/admin/app/layout").addAllObjects(model);
	}
	
	@RequestMapping(value = "/recommend", method = RequestMethod.GET)
	public String recommend(HttpServletRequest request, ModelMap model) {
		int pn = ServletRequestUtils.getIntParameter(request, "pn", 1);
		Page<App> page = appService.listRecommend(pn);
		request.setAttribute("page", page);
		request.setAttribute("count", appService.countRecommend());
		request.setAttribute("from", "recommend");
		return "/admin/app/layout";
	}
	
	// 以下主要用来展示页面
	@RequestMapping(value = "/{var}", method = RequestMethod.GET)
	public String forward(@PathVariable(value="var") String var) {
		return "admin/app/" + var;
	}
	
//	@RequestMapping(value = "admin/app/index", method = RequestMethod.GET)
//	public void index() {
//	}
//	
//	@RequestMapping(value = "admin/app/top", method = RequestMethod.GET)
//	public void top() {
//	}
//	
//	@RequestMapping(value = "admin/app/center", method = RequestMethod.GET)
//	public void center() {
//	}
//	
//	@RequestMapping(value = "admin/app/down", method = RequestMethod.GET)
//	public void down() {
//	}
//	
//	@RequestMapping(value = "admin/app/middel", method = RequestMethod.GET)
//	public void middel() {
//	}
//	
//	@RequestMapping(value = "admin/app/left", method = RequestMethod.GET)
//	public void left() {
//	}
	
}
