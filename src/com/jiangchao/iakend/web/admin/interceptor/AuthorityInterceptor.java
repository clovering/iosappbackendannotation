package com.jiangchao.iakend.web.admin.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.extern.log4j.Log4j;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

@Log4j
public class AuthorityInterceptor extends HandlerInterceptorAdapter {

	private static final String LOGIN_URL = "/admin/login";

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		log.debug(String.format("requestUrl: %s", request.getServletPath()));
		if (request.getServletPath().startsWith(LOGIN_URL)) {
			return true;
		}
		if (request.getSession().getAttribute("userName") != null) {
			log.debug(String.format("currentUser: %s", request.getSession().getAttribute("userName")));
			return true;
		}
		response.sendRedirect(request.getContextPath() + LOGIN_URL);
		return false;
	}

}
