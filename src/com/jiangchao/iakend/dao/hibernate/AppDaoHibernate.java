package com.jiangchao.iakend.dao.hibernate;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.jiangchao.iakend.dao.IAppDao;
import com.jiangchao.iakend.model.App;
import com.jiangchao.iakend.model.AppQuery;
import com.jiangchao.iakend.model.AppSearch;
import com.jiangchao.scaffold.dao.hibernate.BaseDaoHibernate;

@Repository("appDao")
public class AppDaoHibernate extends BaseDaoHibernate<App, Integer> implements IAppDao {

	private static final String HQL_LIST = "from App ";
	private static final String HQL_COUNT = "select count(*) ";
	private static final String HQL_ORDER_BY = "order by displayOrder desc, createdAt desc";
	private static final String HQL_LIST_ALL = HQL_LIST + HQL_ORDER_BY;
	private static final String HQL_LIST_BY_PRICETREND = HQL_LIST + "where priceTrend = ? " + HQL_ORDER_BY;
	private static final String HQL_COUNT_BY_PRICETREND = HQL_COUNT + HQL_LIST_BY_PRICETREND;

	private static final String HQL_LIST_BY_NAME = HQL_LIST + "where name like ? " + HQL_ORDER_BY;
	private static final String HQL_LIST_BY_APPLICATIONID = HQL_LIST + "where applicationId = ? " + HQL_ORDER_BY;
	private static final String HQL_COUNT_BY_NAME = HQL_COUNT + HQL_LIST_BY_NAME;
	private static final String HQL_COUNT_BY_APPLICATIONID = HQL_COUNT + HQL_LIST_BY_APPLICATIONID;

	@Override
	public List<App> queryByName(AppSearch appSearch) {
		return list(HQL_LIST_BY_NAME, getLikeParam(appSearch.getQuery()));
	}

	@Override
	public List<App> queryByApplicationId(AppSearch appSearch) {
		return list(HQL_LIST_BY_APPLICATIONID, getQueryParam(Integer.parseInt(appSearch.getQuery())));
	}

	@Override
	public int countyByName(AppSearch appSearch) {
		return this.<Number> aggregate(HQL_COUNT_BY_NAME, getLikeParam(appSearch.getQuery())).intValue();
	}

	@Override
	public int countByApplicationId(AppSearch appSearch) {
		return this.<Number> aggregate(HQL_COUNT_BY_APPLICATIONID,
				getQueryParam(Integer.parseInt(appSearch.getQuery()))).intValue();
	}

	/**
	 * 覆盖父类的listAll()方法, 添加排序
	 */
	@Override
	public List<App> listAll() {
		return list(HQL_LIST_ALL);
	}

	@Override
	public List<App> listByPriceTrend(AppQuery appQuery) {
		return list(HQL_LIST_BY_PRICETREND, getQueryParam(appQuery.getPriceTrend()));
	}

	@Override
	public int countByPriceTrend(AppQuery appQuery) {
		return this.<Number> aggregate(HQL_COUNT_BY_PRICETREND, getQueryParam(appQuery.getPriceTrend())).intValue();
	}

	// 当做Object传入, 在这之前一定要做好类型转换!
	private Object[] getLikeParam(Object query) {
		return new Object[] { "%" + query + "%" };
	}

	private Object[] getQueryParam(Object query) {
		return new Object[] { query };
	}
	
	/**
	 * 覆盖父类的listAll()方法, 添加排序(需要分页)
	 */
	@Override
	public List<App> listAll(int pn, int pageSize) {
		return list(HQL_LIST_ALL, pn, pageSize);
	}

	@Override
	public List<App> listByPriceTrend(int pn, int pageSize, AppQuery appQuery) {
		return list(HQL_LIST_BY_PRICETREND, pn, pageSize, getQueryParam(appQuery.getPriceTrend()));
	}

}
