package com.jiangchao.iakend.dao;

import java.util.List;

import com.jiangchao.iakend.model.App;
import com.jiangchao.iakend.model.AppQuery;
import com.jiangchao.iakend.model.AppSearch;
import com.jiangchao.scaffold.dao.IBaseDao;

public interface IAppDao extends IBaseDao<App, Integer> {
	
	public int countyByName(AppSearch appSearch);

	public int countByApplicationId(AppSearch appSearch);
	
	public List<App> queryByName(AppSearch appSearch);

	public List<App> queryByApplicationId(AppSearch appSearch);

	public List<App> listByPriceTrend(AppQuery appQuery);
	
	public int countByPriceTrend(AppQuery appQuery);

	public List<App> listByPriceTrend(int pn, int pageSize, AppQuery appQuery);

}
