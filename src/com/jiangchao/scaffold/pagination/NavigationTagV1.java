package com.jiangchao.scaffold.pagination;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.TagSupport;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.taglibs.standard.tag.common.core.UrlSupport;

/**
 * 自定义分页标签V1, 样式为"上一页,下一页,跳转".
 */
@Setter
@Getter
@Log4j
public class NavigationTagV1 extends TagSupport {

	private static final long serialVersionUID = 5094772377121780054L;

	/** request中用于保存Page<E>对象的变量名, 默认为"page". */
	private String bean = "page";

	/** 分页跳转的url地址, 此属性必须. */
	private String url = null;

	/** 是否显示跳转. */
	private boolean showJump = true;

	/** 是否优化分页,仅对主键为数值型的有效. */
	private boolean optimize = false;

	@Override
	public int doStartTag() throws JspException {
		JspWriter writer = pageContext.getOut();
		Page<?> onePage = (Page<?>) pageContext.getRequest().getAttribute(bean);
		if (onePage == null) {
			return SKIP_BODY;
		}

		url = resolveUrl(url, pageContext);
		Object firstModel = null;
		Object lastModel = null;
		if (onePage.getItems() != null && !onePage.getItems().isEmpty()) {
			firstModel = onePage.getItems().get(0);
			lastModel = onePage.getItems().get(onePage.getItems().size() - 1);
		}
		try {
			// 如果不是首页
			if (onePage.isHasPre()) {
				String preUrl = append(url, "pn", onePage.getIndex() - 1);
				if (optimize && firstModel != null) {
					preUrl = append(preUrl, PageUtil.getIdName(firstModel), PageUtil.getIdValue(firstModel));
					preUrl = append(preUrl, "pre", "true");
				}
				writer.print("<a href=\"" + preUrl + "\">上一页</a>&nbsp;");
			}
			// 如果不是尾页
			if (onePage.isHasNext()) {
				String nextUrl = append(url, "pn", onePage.getIndex() + 1);
				if (optimize && lastModel != null) {
					nextUrl = append(nextUrl, PageUtil.getIdName(lastModel), PageUtil.getIdValue(lastModel));
				}
				writer.print("<a href=\"" + nextUrl + "\">下一页</a><br/>");
				if (showJump) {
					writer.print(makeNext(onePage, url));
				}
			}
		} catch (Exception e) {
			log.error(ExceptionUtils.getStackTrace(e));
		}

		return SKIP_BODY;
	}

	private String makeNext(Page<?> page, String url) {
		StringBuffer sb = new StringBuffer("");
		sb.append("<input type=\"text\" name=\"pn\" size=\"3\" value=\"1\" format=\"*N\"/>").append(page.getIndex())
				.append("/").append(page.getContext().getPageCount()).append("页<anchor>跳转<go href=\"").append(url)
				.append("\" method=\"get\" accept-charset=\"utf-8\">")
				.append("<postfield name=\"pn\" value=\"$pn\"/></go></anchor><br/>");
		return sb.toString();
	}

	private String append(String url, String key, int value) {
		return append(url, key, String.valueOf(value));
	}

	/**
	 * 为url 参加参数对
	 */
	private String append(String url, String key, String value) {
		if (url == null || url.trim().isEmpty()) {
			return "";
		}

		if (url.indexOf("?") == -1) {
			url = url + "?" + key + "=" + value;
		} else {
			url = url + "&amp;" + key + "=" + value;
		}

		return url;
	}

	/**
	 * 为url添加上下文环境, 如果是登陆用户则还要添加uid参数.
	 */
	private String resolveUrl(String url, PageContext pageContext) throws JspException {
		url = UrlSupport.resolveUrl(url, null, pageContext);
		url = url.replaceAll("&pn=\\d*", "").replaceAll("&pre=true", "").replaceAll("&id=\\d*", "");
		return url;
	}

}
