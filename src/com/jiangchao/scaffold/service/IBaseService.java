package com.jiangchao.scaffold.service;

import java.io.Serializable;
import java.util.List;

import com.jiangchao.scaffold.pagination.Page;

public interface IBaseService<M extends Serializable, PK extends Serializable> {

	public M save(M model);

	public void saveOrUpdate(M model);

	public void update(M model);

	public void merge(M model);

	public void delete(PK id);

	public M get(PK id);

	public int countAll();

	public List<M> listAll();
	
	public Page<M> listAll(int pn);
    
    public Page<M> listAll(int pn, int pageSize);
    
    public Page<M> pre(PK pk, int pn);
    
    public Page<M> pre(PK pk, int pn, int pageSize);
    
    public Page<M> next(PK pk, int pn);
    
    public Page<M> next(PK pk, int pn, int pageSize);
    
}
