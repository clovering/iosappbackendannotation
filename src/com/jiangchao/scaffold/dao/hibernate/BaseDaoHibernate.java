package com.jiangchao.scaffold.dao.hibernate;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.util.Assert;

import com.jiangchao.scaffold.dao.IBaseDao;
import com.jiangchao.scaffold.pagination.PageUtil;

//TODO 这里的get和set方法用于哪个需要确认下, 去掉可不可以?!
@Setter
@Getter
public abstract class BaseDaoHibernate<M extends Serializable, PK extends Serializable> implements IBaseDao<M, PK> {

	private Class<M> entityClass;
	private String HQL_LIST_ALL;
	private String HQL_COUNT_ALL;
	private String HQL_OPTIMIZE_PRE_LIST_ALL;
	private String HQL_OPTIMIZE_NEXT_LIST_ALL;
	private String pkName = null;
	@Autowired
	@Qualifier("sessionFactory")
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public BaseDaoHibernate() { // 对应spring-dao.xml中的init-method="init", 不过这个是注解版所以没有了, 并且这里的名字也不用叫init了, 具体对比xml版吧
		this.entityClass = (Class<M>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0]; // 实体Class
		String entityName = this.entityClass.getSimpleName(); // 实体类名
		Field[] fields = this.entityClass.getDeclaredFields();	// 字段名
		for (Field f : fields) {
			if (f.isAnnotationPresent(Id.class)) {
				this.pkName = f.getName();
			}
		}
		Assert.notNull(pkName);
		
		HQL_LIST_ALL = "from " + entityName; // from xxx.yyy.zzz.App
		HQL_COUNT_ALL = " select count(*) from " + entityName; // select count(*) from xxx.yyy.zzz.App
		HQL_OPTIMIZE_PRE_LIST_ALL = "from " + entityName + " where " + pkName + " > ? order by " + pkName + " asc";
		HQL_OPTIMIZE_NEXT_LIST_ALL = "from " + entityName + " where " + pkName + " < ? order by " + pkName + " desc";
	}
	
	public Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	@SuppressWarnings("unchecked")
	@Override
	public PK save(M model) {
		return (PK) getSession().save(model);
	}

	@Override
	public void saveOrUpdate(M model) {
		getSession().saveOrUpdate(model);
	}

	@Override
	public void update(M model) {
		getSession().update(model);
	}
	
	@Override
	public void merge(M model) {
		getSession().merge(model);
	}

	@Override
	public void delete(PK id) {
		getSession().delete(this.get(id));
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public M get(PK id) {
		return (M) getSession().get(this.entityClass, id);
	}

	@Override
	public int countAll() {
		Number total = aggregate(HQL_COUNT_ALL);
		return total.intValue();
	}
	
	@Override
	public List<M> listAll() {
		return list(HQL_LIST_ALL);
	}
	
	@SuppressWarnings("unchecked")
	protected <T> T aggregate(final String hql, final Object... paramlist) {
		Query query = getSession().createQuery(hql);
		setParameters(query, paramlist);
		
		return (T) query.uniqueResult();
	}
	
	protected <T> List<T> list(final String hql, final Object... paramlist) {
		return list(hql, -1, -1, paramlist);
	}
	
	@SuppressWarnings("unchecked")
	protected <T> List<T> list(final String hql, final int pn, final int pageSize, final Object... paramlist) {
		Query query = getSession().createQuery(hql);
        setParameters(query, paramlist);
        if (pn > -1 && pageSize > -1) {
            query.setMaxResults(pageSize);
            int start = PageUtil.getPageStart(pn, pageSize);
            if (start != 0) {
                query.setFirstResult(start);
            }
        }
        if (pn < 0) {
            query.setFirstResult(0);
        }
        List<T> results = query.list();
        return results;
	}
	
	protected void setParameters(Query query, Object[] paramlist) {
		if (paramlist != null) {
			for (int i = 0; i < paramlist.length; i++) {
				if (paramlist[i] instanceof Date) {
					query.setTimestamp(i, (Date) paramlist[i]);//TODO 难道这是bug 使用setParameter不行??
				} else {
					query.setParameter(i, paramlist[i]);
				}
			}
		}
	}

	@Override
	public List<M> listAll(int pn, int pageSize) {
		return list(HQL_LIST_ALL, pn, pageSize);
	}

	@Override
	public List<M> pre(PK pk, int pn, int pageSize) {
		if (pk == null) {
			return list(HQL_LIST_ALL, pn, pageSize);
		}
		// 倒序，重排
		List<M> result = list(HQL_OPTIMIZE_PRE_LIST_ALL, 1, pageSize, pk);
		Collections.reverse(result);
		return result;
	}

	@Override
	public List<M> next(PK pk, int pn, int pageSize) {
		if (pk == null) {
			return list(HQL_LIST_ALL, pn, pageSize);
		}
		return list(HQL_OPTIMIZE_NEXT_LIST_ALL, 1, pageSize, pk);
	}
	
}
